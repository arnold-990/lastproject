﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Movement : MonoBehaviour
{
    public Transform Begin;
    public Transform End;

    [SerializeField] [Range(1, 10)] private int speed = 2;
    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime, Space.World);
    }
}