﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    bool tap;
    public int Score;
    public Text ScoreTxt;
    private void Start()
    {
        Physics.gravity = new Vector3(0, -10.0f, 0);
    }
    private void Update()
    {
        ScoreTxt.text = "Score: " + Score;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(!tap)
            {
                tap = true;
                Physics.gravity = new Vector3(0, 10.0f, 0);
            }
            else
            {
                tap = false;
                Physics.gravity = new Vector3(0, -10.0f, 0);
            }
        }
    }
}