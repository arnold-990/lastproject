﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageGeneration : MonoBehaviour
{
    [SerializeField] GameObject[] Platforms;
    int vectorX = 102;

    private void Start()
    {
        StartCoroutine(StartGenerate());
    }
    void generateLevel()
    {
        int index = Random.Range(0, Platforms.Length);
        Instantiate(Platforms[index], new Vector3(vectorX, 0, 0), Quaternion.identity, GameObject.Find("Stages").transform);
        vectorX += 30;
    }

    IEnumerator StartGenerate()
    {
        while (true)
        {
            generateLevel();
            yield return new WaitForSeconds(3f);
        }
    }
}