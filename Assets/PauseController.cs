﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    public Image PauseScreen;
    public Button PauseButton;
    public void pause()
    {
        PauseScreen.gameObject.SetActive(true);
        PauseButton.gameObject.SetActive(false);
        Time.timeScale = 0;
    }
    public void countinue()
    {
        PauseScreen.gameObject.SetActive(false);
        PauseButton.gameObject.SetActive(true);
        Time.timeScale = 1;
    }
}